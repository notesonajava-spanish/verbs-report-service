package com.notesonjava.spanish.model;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerbAnswer {
	
	private String verb;
	private String username;
	
	private String answer;
	
	private ZonedDateTime answerTime;
	
	private int score;
	
	public VerbAnswer(String spanish, String username){
		this.verb = spanish;
		this.setUsername(username);
		this.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
	}
	
	@JsonIgnore
	public LocalDate getAnswerDay(){
		return answerTime.toLocalDate();
	}
	
}