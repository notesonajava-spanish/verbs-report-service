package com.notesonjava.spanish;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.auth.QuizzAccessManager;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.verbs.VerbAnswerRepository;
import com.notesonjava.spanish.verbs.VerbReportController;
import com.notesonjava.spanish.verbs.VerbReportService;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerbReportApplication {
	

	public static String remoteConnectionString(String user, String password, String host, String db) {
		return "mongodb+srv://"+user+":"+password+"@"+host+"/"+db;	
	}
	
	public static String localConnectionString(String host, int port) {
		return "mongodb://"+host+":"+port;
	}

	public static void main(String[] args) throws IOException, TimeoutException {
		PropertyMap prop = PropertyLoader.loadProperties();
		String portValue = prop.get("server.port").orElse("8080");	
		String mongoPort = prop.get("mongo.port").orElse("27017");
		String mongoHost = prop.get("mongo.host").orElse("localhost");
		String mongoDb = prop.get("mongo.db").orElse("answers");
		String authUserUrl = prop.get("auth.user.url").orElse("https://fmottard.auth0.com/userinfo");
		int dbPort = Integer.parseInt(mongoPort);
		
		MongoClient client = MongoClients.create(localConnectionString(mongoHost, dbPort));
		MongoDatabase database = client.getDatabase(mongoDb);
		
		VerbAnswerRepository verbRepo = new VerbAnswerRepository(database);
	
    	log.info("Create the queues");
    	
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		VerbReportService verbService = new VerbReportService(verbRepo);
		UserHandler userHandler = new UserHandler();
		
		VerbReportController verbController = new VerbReportController(verbService, userHandler);
		
		
		Javalin app = Javalin.create()
				.enableCorsForAllOrigins()
				.port(Integer.parseInt(portValue));
		
		app.defaultContentType("application/json;charset=utf-8");
		app.requestLogger((ctx, timeMs) -> {
		    System.out.println(ctx.method() + " "  + ctx.path() + " took " + timeMs + " ms");
		    // prints "GET /hello took 4.5 ms"
		});
		
		app.get("/ping", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
		
		QuizzAccessManager accessManager = new QuizzAccessManager(authUserUrl, mapper);
		
		app.accessManager((handler, ctx, permittedRoles) -> accessManager.manage(handler, ctx, permittedRoles));	
		verbController.init(app);
		app.start();
		
	}
}

