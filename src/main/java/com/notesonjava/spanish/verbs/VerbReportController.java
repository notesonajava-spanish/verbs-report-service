package com.notesonjava.spanish.verbs;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.notesonjava.auth.UserHandler;
import com.notesonjava.spanish.model.WordStats;

import io.javalin.Javalin;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbReportController {
	
	private VerbReportService verbService;
	private UserHandler userHandler;
	
	public void init(Javalin app) {
		
		app.get("/verbs/ping", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
			
		app.get("/verbs/report", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<String> verbs = ctx.queryParams("verb");
				if(verbs.isEmpty()) {
					List<WordStats<String>> report = verbService.verbReport(user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet();
					ctx.json(report);
				} else {
					List<WordStats<String>> report = verbService.verbReport(verbs, user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet();
					ctx.json(report);
				}
			}
		});
		
		app.get("/verbs/report/daily", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {		
				String daysParam = ctx.queryParam("numDays", "7");
				int numDays = Integer.parseInt(daysParam);
				List<WordStats<LocalDate>> result = verbService.getDailyReport(numDays, user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet();
				ctx.status(200);
				ctx.json(result);						
			}
		});		
	}
}
