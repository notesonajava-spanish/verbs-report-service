package com.notesonjava.spanish;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.hamcrest.MockitoHamcrest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.spanish.verbs.VerbAnswerRepository;
import com.notesonjava.spanish.verbs.VerbReportController;
import com.notesonjava.spanish.verbs.VerbReportService;

import io.javalin.Context;
import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.HttpStatus;

public class VerbReportControllerTest {
	
	private static Javalin app;
	private static VerbAnswerRepository verbRepo;
	
	@BeforeClass
	public static void init() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		
		app = Javalin.create().port(8090);

		UserHandler handler = Mockito.mock(UserHandler.class);
		
		
		Mockito.when(handler.retrieveUser(MockitoHamcrest.argThat(Matchers.any(Context.class)))).thenReturn(Optional.of("abcd"));
		
		verbRepo = Mockito.mock(VerbAnswerRepository.class);
		VerbReportService verbService = new VerbReportService(verbRepo);
		VerbReportController controller = new VerbReportController(verbService, handler);
		controller.init(app);
		app.start();
	}
	
	@AfterClass
	public static void clear() {
		app.stop();
	}
	
	@Test
	@Ignore
	public void saveVerbAnswers() throws IOException {
		try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("verbAnswers.json")){
			String body = IOUtils.toString(is, "UTF-8");
			HttpResponse response = HttpRequest.post("http://localhost:8090/verbs/answers").contentType("application/json").body(body).send();
			Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_CREATED);
			
		}
	}
	
	@Test
	public void getReport() {
	}
	
}
